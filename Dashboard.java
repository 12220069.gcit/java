import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Dashboard extends JFrame implements ActionListener {

    public Dashboard() {
        super("Hotel Management Dashboard");

        setLayout(new GridLayout(4, 2)); // Increase the number of rows for the new button

        // Buttons for various functionalities
        JButton addEmployeeButton = createButton("Add Employee");
        JButton searchRoomButton = createButton("Search Room");
        JButton addCustomerButton = createButton("Add Customer");
        JButton checkoutButton = createButton("Checkout");

        // Adding action listeners to the buttons
        addEmployeeButton.addActionListener(this);
        searchRoomButton.addActionListener(this);
        addCustomerButton.addActionListener(this);
        checkoutButton.addActionListener(this);

        // Button for updating room status
        JButton updateRoomStatusButton = createButton("Update Room Status");
        updateRoomStatusButton.addActionListener(this);

        // Adding buttons to the layout
        add(addEmployeeButton);
        add(addCustomerButton);
        add(searchRoomButton);
        add(updateRoomStatusButton);
        add(checkoutButton);
        

        // Setting frame properties
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 500); // Increase the frame size to accommodate the new button
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private JButton createButton(String text) {
        JButton button = new JButton(text);
        button.setFont(new Font("Arial", Font.PLAIN, 18));
        return button;
    }

    public void actionPerformed(ActionEvent ae) {
        String command = ae.getActionCommand();

        // Handling actions for each button
        switch (command) {
            case "Add Employee":
                JOptionPane.showMessageDialog(this, "Opening Add Employee Form");
                new AddEmployeePage().setVisible(true);
                break;
            case "Search Room":
                JOptionPane.showMessageDialog(this, "Opening Search Room");
                new SearchRoomPage().setVisible(true);
                break;
            case "Add Customer":
                JOptionPane.showMessageDialog(this, "Opening Add Customer Form");
                new AddCustomerPage().setVisible(true);
                break;
            case "Update Room Status":
                JOptionPane.showMessageDialog(this, "Updating Room Status");
                break;
            case "Checkout":
                JOptionPane.showMessageDialog(this, "Opening Checkout");
                new CheckoutPage().setVisible(true);
                break;
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new Dashboard());
    }
}

class AddEmployeePage extends JFrame {

    public AddEmployeePage() {
        super("Add Employee Form");
        AddEmployee addEmployee = new AddEmployee();
        addEmployee.loadEmployeeData();
    }
}
class AddCustomerPage extends JFrame {

    public AddCustomerPage() {
        super("Add Customer Form");
        AddCustomer addCustomer = new AddCustomer();
        addCustomer.loadCustomerData();
        dispose();
    }
}
class SearchRoomPage extends JFrame {

    public SearchRoomPage() {
        super("Search Room");

        SearchRoom searchRoom = new SearchRoom();
        searchRoom.main(null);

        // Set the default close operation for this SearchRoomPage
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Add a window listener to detect when the SearchRoomPage is closed
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosed(java.awt.event.WindowEvent windowEvent) {
                // Do any cleanup or additional actions if needed
                System.out.println("SearchRoomPage closed");
            }
        });

        // Set the visibility of SearchRoom
        searchRoom.setVisible(true);
    }
}
class CheckoutPage extends JFrame {

    public CheckoutPage() {
        super("Checkout");

        // Add components and logic for the Checkout page
        // For example, you can add labels, text fields, and checkout buttons

        setSize(400, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
}
