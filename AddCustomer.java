import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;

class CustomerInfo {
    private String id;
    private int number;
    private String name;
    private String gender;
    private String country;
    private String roomNumber;
    private String checkedIn;
    private String deposit;

    public CustomerInfo(String id, int number, String name, String gender, String country, String roomNumber, String checkedIn, String deposit) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.gender = gender;
        this.country = country;
        this.roomNumber = roomNumber;
        this.checkedIn = checkedIn;
        this.deposit = deposit;
    }

    public String getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getCountry() {
        return country;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public String getCheckedIn() {
        return checkedIn;
    }

    public String getDeposit() {
        return deposit;
    }
}

public class AddCustomer extends JFrame {
    private ArrayList<CustomerInfo> customers;
    private DefaultTableModel tableModel;
    private JTable customerTable;
    private JTextArea displayArea;

    public AddCustomer() {
        customers = new ArrayList<>();
        tableModel = new DefaultTableModel();
        customerTable = new JTable(tableModel);
        displayArea = new JTextArea();

        initializeUI();
    }

    private void initializeUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setSize(800, 600);

        // Create a panel for buttons and input fields
        JPanel inputPanel = new JPanel();
        add(inputPanel, BorderLayout.NORTH);

        // Add components to the input panel
        JButton addButton = new JButton("Add Customer");
        addButton.addActionListener(e -> loadCustomerData());

        JButton displayButton = new JButton("Display Customer Data");
        displayButton.addActionListener(e -> displayCustomerData());

        inputPanel.add(addButton);
        inputPanel.add(displayButton);

        // Create a panel for displaying the data
        JPanel displayPanel = new JPanel(new GridLayout(1, 2));
        add(displayPanel, BorderLayout.CENTER);

        // Create a scroll pane for the JTextArea
        JScrollPane textAreaScrollPane = new JScrollPane(displayArea);
        displayPanel.add(textAreaScrollPane);

        // Create a scroll pane for the JTable
        JScrollPane tableScrollPane = new JScrollPane(customerTable);
        displayPanel.add(tableScrollPane);

        // Set up the table model with column names
        String[] columnNames = {"ID", "Number", "Name", "Gender", "Country", "Room Number", "Checked In", "Deposit"};
        tableModel.setColumnIdentifiers(columnNames);

        setVisible(true);
    }

    public void loadCustomerData() {
        String id = JOptionPane.showInputDialog("Enter ID:");
        int number = Integer.parseInt(JOptionPane.showInputDialog("Enter Number:"));
        String name = JOptionPane.showInputDialog("Enter Name:");
        String gender = JOptionPane.showInputDialog("Enter Gender:");
        String country = JOptionPane.showInputDialog("Enter Country:");
        String roomNumber = JOptionPane.showInputDialog("Enter Room Number:");
        String checkedIn = JOptionPane.showInputDialog("Enter Checked In:");
        String deposit = JOptionPane.showInputDialog("Enter Deposit:");

        customers.add(new CustomerInfo(id, number, name, gender, country, roomNumber, checkedIn, deposit));
        updateTableModel();
        displayCustomerData();
    }

    public void displayCustomerData() {
        // Display data in the JTextArea
        displayArea.setText("");

        for (CustomerInfo customer : customers) {
            displayArea.append("ID: " + customer.getId() +
                    ", Number: " + customer.getNumber() +
                    ", Name: " + customer.getName() +
                    ", Gender: " + customer.getGender() +
                    ", Country: " + customer.getCountry() +
                    ", Room Number: " + customer.getRoomNumber() +
                    ", Checked In: " + customer.getCheckedIn() +
                    ", Deposit: " + customer.getDeposit() + "\n");
        }

        // Update the JTable
        updateTableModel();
    }

    private void updateTableModel() {
        // Clear the existing data in the table model
        tableModel.setRowCount(0);

        // Add data from the ArrayList to the table model
        for (CustomerInfo customer : customers) {
            Object[] rowData = {
                    customer.getId(),
                    customer.getNumber(),
                    customer.getName(),
                    customer.getGender(),
                    customer.getCountry(),
                    customer.getRoomNumber(),
                    customer.getCheckedIn(),
                    customer.getDeposit()
            };
            tableModel.addRow(rowData);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new AddCustomer());
    }
}
