import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class SearchRoom extends JFrame {
    private JPanel contentPane;
    private JTable table;
    private Choice c1;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    SearchRoom frame = new SearchRoom();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void close() {
        this.dispose();
    }

    public SearchRoom() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(530, 200, 700, 500);
        contentPane = new JPanel();
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblSearchForRoom = new JLabel("Search For Room");
        lblSearchForRoom.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblSearchForRoom.setBounds(250, 11, 186, 31);
        contentPane.add(lblSearchForRoom);

        JLabel lblRoomAvailable = new JLabel("Room Bed Type:");
        lblRoomAvailable.setBounds(50, 73, 96, 14);
        contentPane.add(lblRoomAvailable);

        // ... (other labels and components)

        c1 = new Choice();
        c1.add("Single");
        c1.add("Double");
        c1.add("All");
        c1.setBounds(153, 70, 120, 20);
        contentPane.add(c1);

        JButton btnSearch = new JButton("Search");
        btnSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Read data and filter based on the selected bed type
                readDataAndFilter("room_details.txt", c1.getSelectedItem());
            }
        });
        btnSearch.setBounds(200, 400, 120, 30);
        btnSearch.setBackground(Color.BLACK);
        btnSearch.setForeground(Color.WHITE);
        contentPane.add(btnSearch);

        // ... (other buttons)

        table = new JTable();
        table.setBounds(0, 187, 700, 300);
        contentPane.add(table);

        // ... (other components)

        getContentPane().setBackground(Color.WHITE);
    }

    private void readDataAndFilter(String fileName, String selectedBedType) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            DefaultTableModel model = new DefaultTableModel();

            // Read column names from the first line
            line = reader.readLine();
            String[] columnNames = line.split("\\s+");
            model.setColumnIdentifiers(columnNames);

            // Read data, filter based on the selected bed type (or display all), and add rows to the model
            while ((line = reader.readLine()) != null) {
                String[] rowData = line.split("\\s+");
                if ("All".equalsIgnoreCase(selectedBedType) || (rowData.length > 1 && rowData[1].equalsIgnoreCase(selectedBedType))) {
                    model.addRow(rowData);
                }
            }

            table.setModel(model);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}