/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class AddRoom {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("ADD ROOM DETAILS");

        System.out.print("Room Number: ");
        String room = scanner.nextLine();

        System.out.print("Availability (Available/Occupied): ");
        String available = scanner.nextLine();

        System.out.print("Cleaning Status (Cleaned/Dirty): ");
        String status = scanner.nextLine();

        System.out.print("Price: ");
        String price = scanner.nextLine();

        System.out.print("Bed Type (Single Bed/Double Bed): ");
        String type = scanner.nextLine();

        // Perform data storage logic here
        // You can store these values in your preferred data structure or database.

         // Store room details in an ArrayList
        ArrayList<String> roomDetails = new ArrayList<>();
        roomDetails.add(room);
        roomDetails.add(available);
        roomDetails.add(status);
        roomDetails.add(price);
        roomDetails.add(type);
        System.out.println("Room Successfully Added");

        scanner.close();
    }
}
