
import java.util.Scanner;

public class UpdateRoom {
    public class RoomManagement {
        public String roomNumber;
        public String bedType;
        public String availability;
        public String cleaningStatus;
        public double price;
    
        public RoomManagement(String roomNumber, String bedType, String availability, String cleaningStatus, double price) {
            this.roomNumber = roomNumber;
            this.bedType = bedType;
            this.availability = availability;
            this.cleaningStatus = cleaningStatus;
            this.price = price;
        }
    }
    
    public static void main(String[] args) {
        int maxRooms = 16; // Define the maximum number of rooms
        RoomManagement[] rooms = new RoomManagement[maxRooms]; // Array to store room information

        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                System.out.println("Update Room Status");
                System.out.println("1. Check Room Status");
                System.out.println("2. Update Room Status");
                System.out.println("3. Back to Main Menu");
                System.out.print("Enter your choice (1-3): ");

                int choice = scanner.nextInt();
                scanner.nextLine(); // Consume the newline character

                switch (choice) {
                    case 1:
                        System.out.print("Enter Guest ID: ");
                        String guestId = scanner.nextLine();
                        RoomManagement room = findRoomByGuestId(rooms, guestId);
                        if (room != null) {
                            System.out.println("Room Number: " + room.roomNumber);
                            System.out.println("Bed Type: " + room.bedType);
                            System.out.println("Availability: " + room.availability);
                            System.out.println("Cleaning Status: " + room.cleaningStatus);
                            System.out.println("Price: " + room.price);
                        } else {
                            System.out.println("Room not found for the given Guest ID.");
                        }
                        break;

                    case 2:
                        System.out.print("Enter Guest ID: ");
                        String guestIdToUpdate = scanner.nextLine();
                        RoomManagement roomToUpdate = findRoomByGuestId(rooms, guestIdToUpdate);
                        if (roomToUpdate != null) {
                            System.out.print("Enter New Availability: ");
                            String newAvailability = scanner.nextLine();
                            System.out.print("Enter New Clean Status: ");
                            String newCleanStatus = scanner.nextLine();

                            // Update the room status based on the provided details
                            roomToUpdate.availability = newAvailability;
                            roomToUpdate.cleaningStatus = newCleanStatus;
                            System.out.println("Room status updated successfully.");
                        } else {
                            System.out.println("Room not found for the given Guest ID.");
                        }
                        break;

                    case 3:
                        // Exit and go back to the main menu
                        System.out.println("Going back to the main menu.");
                        return;

                    default:
                        System.out.println("Invalid choice. Please enter 1, 2, or 3.");
                        break;
                }
            }
        }
    }

    // Helper method to find a room by guest ID
    private static RoomManagement findRoomByGuestId(RoomManagement[] rooms, String guestId) {
        for (RoomManagement room : rooms) {
            if (room != null && guestId.equals(room.roomNumber)) {
                return room;
            }
        }
        return null;
    }
}
