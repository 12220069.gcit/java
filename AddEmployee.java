import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.table.DefaultTableModel;
class EmployeeInfo {
    private String name;
    private int age;
    private String gender;
    private String job;
    private double salary;
    private String phone;
    private String cid;
    private String gmail;

    public EmployeeInfo(String name, int age, String gender, String job, double salary, String phone, String cid, String gmail) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.job = job;
        this.salary = salary;
        this.phone = phone;
        this.cid = cid;
        this.gmail = gmail;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getJob() {
        return job;
    }

    public double getSalary() {
        return salary;
    }

    public String getPhone() {
        return phone;
    }

    public String getCid() {
        return cid;
    }

    public String getGmail() {
        return gmail;
    }
}

public class AddEmployee extends JFrame {
    private ArrayList<EmployeeInfo> employees;
    private DefaultTableModel tableModel;
    private JTable employeeTable;
    private JTextArea displayArea;

    public AddEmployee() {
        employees = new ArrayList<>();
        tableModel = new DefaultTableModel();
        employeeTable = new JTable(tableModel);
        displayArea = new JTextArea();

        initializeUI();
    }

    private void initializeUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setSize(800, 600);

        // Create a panel for buttons and input fields
        JPanel inputPanel = new JPanel();
        add(inputPanel, BorderLayout.NORTH);

        // Add components to the input panel
        JButton addButton = new JButton("Add Employee");
        addButton.addActionListener(e -> loadEmployeeData());

        JButton displayButton = new JButton("Display Employee Data");
        displayButton.addActionListener(e -> displayEmployeeData());

        inputPanel.add(addButton);
        inputPanel.add(displayButton);

        // Create a panel for displaying the data
        JPanel displayPanel = new JPanel(new GridLayout(1, 2));
        add(displayPanel, BorderLayout.CENTER);

        // Create a scroll pane for the JTextArea
        JScrollPane textAreaScrollPane = new JScrollPane(displayArea);
        displayPanel.add(textAreaScrollPane);

        // Create a scroll pane for the JTable
        JScrollPane tableScrollPane = new JScrollPane(employeeTable);
        displayPanel.add(tableScrollPane);

        // Set up the table model with column names
        String[] columnNames = {"Name", "Age", "Gender", "Job", "Salary", "Phone", "CID", "Email"};
        tableModel.setColumnIdentifiers(columnNames);

        setVisible(true);
    }

    public void loadEmployeeData() {
        String name = JOptionPane.showInputDialog("Enter Name:");
        int age = Integer.parseInt(JOptionPane.showInputDialog("Enter Age:"));
        String gender = JOptionPane.showInputDialog("Enter Gender (Male/Female):");
        String job = JOptionPane.showInputDialog("Enter Job:");
        double salary = Double.parseDouble(JOptionPane.showInputDialog("Enter Salary:"));
        String phone = JOptionPane.showInputDialog("Enter Phone:");
        String cid = JOptionPane.showInputDialog("Enter CID:");
        String email = JOptionPane.showInputDialog("Enter Email:");

        employees.add(new EmployeeInfo(name, age, gender, job, salary, phone, cid, email));
        updateTableModel();
        displayEmployeeData();
    }
    public void displayEmployeeData() {
        // Display data in the JTextArea
        displayArea.setText("");

        for (EmployeeInfo employee : employees) {
            displayArea.append("Name: " + employee.getName() +
                    ", Age: " + employee.getAge() +
                    ", Gender: " + employee.getGender() +
                    ", Job: " + employee.getJob() +
                    ", Salary: " + employee.getSalary() +
                    ", Phone: " + employee.getPhone() +
                    ", CID: " + employee.getCid() +
                    ", Email: " + employee.getGmail() + "\n");
        }

        // Update the JTable
        updateTableModel();
    }

    private void updateTableModel() {
        // Clear the existing data in the table model
        tableModel.setRowCount(0);

        // Add data from the ArrayList to the table model
        for (EmployeeInfo employee : employees) {
            Object[] rowData = {
                    employee.getName(),
                    employee.getAge(),
                    employee.getGender(),
                    employee.getJob(),
                    employee.getSalary(),
                    employee.getPhone(),
                    employee.getCid(),
                    employee.getGmail()
            };
            tableModel.addRow(rowData);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new AddEmployee());
    }
}